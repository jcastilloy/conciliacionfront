// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBO5spHq1BXHzuipF4TFBdYFqTpYKtTTF8",
    authDomain: "conciliacion-contable-a6e5e.firebaseapp.com",
    databaseURL: "https://conciliacion-contable-a6e5e.firebaseio.com",
    projectId: "conciliacion-contable-a6e5e",
    storageBucket: "conciliacion-contable-a6e5e.appspot.com",
    messagingSenderId: "893704630511",
    appId: "1:893704630511:web:5d3df51ee9184208452b1a",
    measurementId: "G-PVY6N19WJG"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
