import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForgotPasswordComponent } from './components/auth/forgot-password/forgot-password.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { VerifyEmailComponent } from './components/auth/verify-email/verify-email.component';



import { CargaExcelComponent } from './components/carga-excel/carga-excel.component';
import { IndexComponent } from './components/index/index.component';
import { DetalleArchivoComponent } from './components/detalle-archivo/detalle-archivo.component';
import { ProdGuard as guard } from './guards/prod.guard';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path:'forgot-password', component:  ForgotPasswordComponent },
  { path:'carga', component: CargaExcelComponent, canActivate: [guard] },
  { path:'index', component: IndexComponent, canActivate: [guard]},
  { path:'detalle/:id', component: DetalleArchivoComponent, canActivate: [guard]},
  { path: '**', redirectTo: '', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
