export class LoginUsuario {
    id: string;
    usuario: string;
    password: string;

    constructor(id: string, usuario: string, password: string) {
        this.id = id;
        this.usuario = usuario;
        this.password = password;
    }
}
