export class JwtDTO {
    id: string;
    token: string;
    data: any
}
