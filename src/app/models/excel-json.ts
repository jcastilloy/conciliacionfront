export interface ArchivoConsolidado {
    ctaContable?: number;
    ref?: string;
    nomContable?: string;
    grupo1?: Grupo1;
    grupo2?: Grupo2OrGrupo3OrGrupo4OrGrupo5;
    grupo3?: Grupo2OrGrupo3OrGrupo4OrGrupo5;
    grupo4?: Grupo2OrGrupo3OrGrupo4OrGrupo5;
    grupo5?: Grupo2OrGrupo3OrGrupo4OrGrupo5;
    grupo6?: Grupo6;
  }
  export interface Grupo1 {
    nomGrupo: string;
    contenido?: (ContenidoEntity)[] | null;
  }
  export interface ContenidoEntity {
    c1?: string | null;
    valor?: number | null;
    c2?: string | null;
    c3?: string | null;
    c4?: string | null;
    c5?: string | null;
    c6?: string | null;
    c7?: string | null;
    c8?: string | null;
    reclasificacion?: ReclasificacionOrAjustesOrAjueste | null;
    ajustes?: ReclasificacionOrAjustesOrAjueste1 | null;
  }
  export interface ReclasificacionOrAjustesOrAjueste {
    debe: number;
    haber: number;
  }
  export interface ReclasificacionOrAjustesOrAjueste1 {
    debe: number;
    haber: number;
  }
  export interface Grupo2OrGrupo3OrGrupo4OrGrupo5 {
    nomGrupo: string;
    contenido?: (ContenidoEntity1)[] | null;
  }
  export interface ContenidoEntity1 {
    c1?: string | null;
    valor?: number | null;
    c2?: string | null;
    c3?: string | null;
    c4?: string | null;
    c5?: string | null;
    c6?: string | null;
    c7?: string | null;
    c8?: string | null;
    cambio?: string | null;
    reclasificacion?: ReclasificacionOrAjustesOrAjueste2 | null;
    ajustes?: ReclasificacionOrAjustesOrAjueste3 | null;
  }
  export interface ReclasificacionOrAjustesOrAjueste2 {
    debe: number;
    haber: number;
  }
  export interface ReclasificacionOrAjustesOrAjueste3 {
    debe: number;
    haber: number;
  }
  export interface Grupo6 {
    nomGrupo: string;
    contenido: Contenido;
  }
  export interface Contenido {
    reclasificacion: ReclasificacionOrAjustesOrAjueste4;
    ajueste: ReclasificacionOrAjustesOrAjueste4;
  }
  export interface ReclasificacionOrAjustesOrAjueste4 {
    debe: number;
    haber: number;
  }
  