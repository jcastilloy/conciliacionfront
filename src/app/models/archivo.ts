export class Archivo {
    id?: number; //el signo ? quiere decir que no es campo obligatorio
    nombre: string;
    extension: string;
    createdAt?: string;
    updatedAt?: string;

    
    constructor(id: number, nombre: string, extension: string, createdAt: string, updatedAt: string){
        this.id = id;
        this.nombre = nombre;
        this.extension = extension;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}


