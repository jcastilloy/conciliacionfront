import { TestBed } from '@angular/core/testing';

import { ArchInterceptorService } from './arch-interceptor.service';

describe('ArchService', () => {
  let service: ArchInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArchInterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
