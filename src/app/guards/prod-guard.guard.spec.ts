import { TestBed } from '@angular/core/testing';

import { ProdGuard } from './prod.guard';

describe('ProdGuardGuard', () => {
  let guard: ProdGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ProdGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
