import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth'
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http'
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { IndexComponent } from './components/index/index.component';
import { LoginComponent } from './components/auth/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { CargaExcelComponent } from './components/carga-excel/carga-excel.component';
import { ForgotPasswordComponent } from './components/auth/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './components/auth/verify-email/verify-email.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { interceptorProvider } from '../app/interceptor/arch-interceptor.service';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';
import { DetalleArchivoComponent } from '././components/detalle-archivo/detalle-archivo.component';



@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    MenuComponent,
    CargaExcelComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    ConfirmationDialogComponent,
    DetalleArchivoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [ConfirmationDialogService, interceptorProvider],
  entryComponents: [ ConfirmationDialogComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
