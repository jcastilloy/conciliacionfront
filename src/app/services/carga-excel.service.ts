import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CargaExcelService {

  archivoURL = 'http://localhost:3000/carga/';
  constructor(
    private httpClient: HttpClient
  ) { }

  public cargar(object: Object):Observable<any>{
    return this.httpClient.post<any>(this.archivoURL + 'cargar-excel', object);
  }

  public lista(id: number):Observable<any>{
    return this.httpClient.get<any[]>(this.archivoURL + `detalle/${id}`);
  }
}
