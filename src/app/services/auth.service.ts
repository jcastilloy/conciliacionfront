import { Injectable, NgZone } from '@angular/core';
import { Router } from  "@angular/router";
import { AngularFireAuth } from  "@angular/fire/auth";
import  firebase  from 'firebase';

import { TokenService } from '../services/token.service';
import { AuthLocalService } from '../services/auth-local.service';
import { LoginUsuario } from '../models/login-usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  userData: any;
  loginUsuario: LoginUsuario;

  constructor(
    private tokenService: TokenService,
    public  afAuth:  AngularFireAuth, 
    public  router:  Router,
    private authService: AuthLocalService,
    public ngZone: NgZone // Servicio NgZone para eliminar la advertencia de alcance externo
  ) { 
    /* Guardar datos de usuario en almacenamiento local cuando
     iniciado sesión y configurando nulo al cerrar sesión */
    this.afAuth.authState.subscribe(user=>{
      if(user){
        this.userData = user;
        this.loginLocal(this.userData);
      }
    });    
  }

  loginLocal(ldata: any){
    this.loginUsuario = new LoginUsuario(ldata.uid, ldata.l, ldata.l);
    this.authService.login(this.loginUsuario).subscribe(
        data => {
          this.tokenService.setToken(data.data.token);
          this.tokenService.setUserName(data.data.id);
      });
  }

  SingIn(email, password){
    return firebase.auth().signInWithEmailAndPassword(email, password)
    .then((result)=>{
      this.ngZone.run(() => {
        window.location.reload();
        this.router.navigate(['/index']);
        this.loginLocal(result.user);
      });
      this.userData=result.user;
    }).catch((error)=>{
      window.alert(error.message);
    });
  }
  // Restablecer contraseña olvidada
  ForgotPassword(passwordResetEmail) {
    return firebase.auth().sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
      window.alert('Se ha enviado un correo electrónico para restablecer su contraseña, revise su bandeja de entrada por favor.');
      this.router.navigate(['login']);
    }).catch((error) => {
      window.alert(error)
    })
  }

  // Devuelve verdadero cuando el usuario inicia sesión y se verifica el correo electrónico
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

   // Cerrar sesión
   SignOut() {
    this.tokenService.logOut();
    return firebase.auth().signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
      
    })
  }

}
