import { TestBed } from '@angular/core/testing';

import { AuthLocalService } from './auth-local.service';

describe('AuthLocalService', () => {
  let service: AuthLocalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthLocalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
