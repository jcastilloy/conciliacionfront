import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

import { Archivo } from '../models/archivo';

@Injectable({
  providedIn: 'root'
})
export class ArchivoService {

  archivoURL = 'http://localhost:3000/archivo/';


  constructor(
    private httpClient: HttpClient
  ) { }

  public lista():Observable<Archivo[]>{
    return this.httpClient.get<Archivo[]>(this.archivoURL + 'archivos');
  }

  public delete(archivo: Archivo): Observable<any>{
    return this.httpClient.post<any>(this.archivoURL + 'eliminar', archivo);
  }

  

}
