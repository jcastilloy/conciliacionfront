import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { LoginUsuario } from '../models/login-usuario'
import { JwtDTO } from '../models/JwtDTO'

@Injectable({
  providedIn: 'root'
})
export class AuthLocalService {

  authURL = 'http://localhost:3000/auth/';

  constructor(
    private httpClient: HttpClient
  ) { }

  public login(loginUsuario: LoginUsuario): Observable<JwtDTO> {
    return this.httpClient.post<JwtDTO>(this.authURL + 'login', loginUsuario);
  }
}
