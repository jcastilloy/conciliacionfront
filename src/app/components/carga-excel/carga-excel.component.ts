import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';


import { headers } from '../../models/encabezados-excel';
import { CargaExcelService } from '../../services/carga-excel.service';

type AOA = any[][];

@Component({
  selector: 'app-carga-excel',
  templateUrl: './carga-excel.component.html',
  styleUrls: ['./carga-excel.component.css']
})
export class CargaExcelComponent implements OnInit {

  data: AOA;
  
  datos={};

  constructor(
    private cargarService: CargaExcelService,
    private toastr: ToastrService, 
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('No se permiten multiples archivos');

    //adquirir nombre y extension del archivo
    
    const reader: FileReader = new FileReader();
    
    reader.onload = (e: any) => {
      
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      
      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      
      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1, defval:0 }));
      
      
      this.crearJsonArchivo(target.files[0], this.data, ws);
    };
    reader.readAsBinaryString(target.files[0]);
  }

  //creacion del json para el archivo
  crearJsonArchivo(archivo, data, ws){
    var json: string; 
    // var objArchivo ={
    //   nombre:(archivo.name.split("."))[0],
    //   extension:(archivo.name.split("."))[1],
    //   estado:true,
    //   grupos: this.nombreGrupos(ws, data)
    // }

    var objArchivo ={
      nombre:(archivo.name.split("."))[0],
      extension:(archivo.name.split("."))[1],
      estado:true,
      grupos: this.nombreGrupos(ws, data)
    }
   
    this.datos["archivo"]=objArchivo;
    json = JSON.stringify(this.datos);
    //console.log(this.datos);
    this.enviarJson(this.datos);

  }

  //creacion del json para los grupos
  nombreGrupos(ws, data){
    var grupos =[];

    //ejecutar y obtener informacion del grupo 1  
    var grupo_1 = ws['D1'];
    var nom_grupo_1 = (grupo_1 ? grupo_1.v : "grupo 1")
    grupos.push({nombre:nom_grupo_1, ArchivoDetalles:this.crearDetalle(data, 1), EncabezadoColumnas:this.crearEncabezados(data, 1)});
    

    //ejecutar y obtener informacion del grupo 2
    var grupo_2 = ws['S1'];
    var nom_grupo_2 = (grupo_2 ? grupo_2.v : "grupo 2")
    grupos.push({nombre:nom_grupo_2, ArchivoDetalles:this.crearDetalle(data, 2), EncabezadoColumnas:this.crearEncabezados(data, 2)});
    

    //ejecutar y obtener informacion del grupo 3
    var grupo_3 = ws['AJ1'];
    var nom_grupo_3 = (grupo_3 ? grupo_3.v : "grupo 3")
    grupos.push({nombre:nom_grupo_3, ArchivoDetalles:this.crearDetalle(data, 3), EncabezadoColumnas:this.crearEncabezados(data, 3)});
    
  
    //ejecutar y obtener informacion del grupo 4
    var grupo_4 = ws['BA1'];
    var nom_grupo_4 = (grupo_4 ? grupo_4.v : "grupo 4")
    grupos.push({nombre:nom_grupo_4, ArchivoDetalles:this.crearDetalle(data, 4), EncabezadoColumnas:this.crearEncabezados(data, 4)});

    //ejecutar y obtener informacion del grupo 5
    var grupo_5 = ws['BR1'];
    var nom_grupo_5 = (grupo_5 ? grupo_5.v : "grupo 5")
    grupos.push({nombre:nom_grupo_5, ArchivoDetalles:this.crearDetalle(data, 5), EncabezadoColumnas:this.crearEncabezados(data, 5)});
  
    //ejecutar y obtener informacion del grupo 6
    var grupo_6 = ws['CI1'];
    var nom_grupo_6 = (grupo_6 ? grupo_6.v : "grupo 6")
    grupos.push({nombre:nom_grupo_6, ArchivoDetalles:this.crearDetalle(data, 6), EncabezadoColumnas:this.crearEncabezados(data, 6)});
   
    return grupos;
   } 
  
   //no tomo el encabezado del resumen ya que no es necesario porque
   //el resumen siempre tendra un debe y haber para ajuste y reclasificacion 
  crearEncabezados(data, grupo){
    
    function armarJsonEncabezado(key, valor){
      var k = key;
      if(!k && contCeldas==0) k = "ctaContable1";
      if(contCeldas==1) k = "ref1";
      if(contCeldas==2) k = "nomContable1";
      return{
        "columna": k.substr(0,k.length-1),
        "descripcion": valor,
        "estado": true
      };
     }

    var contFilas=0;
    var contCeldas=0;
    var encabezados=[];

    data.forEach(function (fila){
      fila.forEach(valor => {
        var key = headers[contCeldas];
        if((contCeldas % 93)==0){
          contCeldas=0;
        }
        if (contFilas==2) {
          if(key!=="-1" && (contCeldas>-1 && contCeldas<3)){
            encabezados.push(armarJsonEncabezado(key, valor));
          }
          switch (grupo){
            case 1:{
              if(key && key!=="-1" && (contCeldas>2 && contCeldas<18)){
                encabezados.push(armarJsonEncabezado(key, valor));
              }
              break;
            }
            case 2:
              if(key && key!=="-1" && (contCeldas>17 && contCeldas<35)){
                encabezados.push(armarJsonEncabezado(key, valor));
              }
            break;
            case 3:
              if(key && key!=="-1" && (contCeldas>34 && contCeldas<52)){
                encabezados.push(armarJsonEncabezado(key, valor));
              }
            break;
            case 4:
              if(key && key!=="-1" && (contCeldas>51 && contCeldas<69)){
                encabezados.push(armarJsonEncabezado(key, valor));
              }
            break;
            case 5:
              if(key && key!=="-1" && (contCeldas>68 && contCeldas<86)){
                encabezados.push(armarJsonEncabezado(key, valor));
              }
            break;
            case 6:
              if(key && key!=="-1" && (contCeldas>85)){
                encabezados.push(armarJsonEncabezado(key, valor));
              }
            break;
          }
        }
        contCeldas++
      });
      contFilas++;
    });
    return encabezados;
  }

  //detalle
  crearDetalle(data, grupo){   
    
    function detallePorGrupo(row, key, valor, contCeldas){
      var k = key;
      if(!k && contCeldas==0) k = "ctaContable1";
      if(contCeldas==1) k = "ref1";
      if(contCeldas==2) k = "nomContable1";
      row[k.substr(0,k.length-1)]=valor;
    }
    
    var contFilas=0;
    var contCeldas=0;
    var filas =[];
       
    data.forEach(function (fila){
      var row={};
      fila.forEach(valor => {
        var key = headers[contCeldas];
        if((contCeldas % 93)==0){
          contCeldas=0;
        }
        if (contFilas >= 3 ){
          if(key!=="-1" && (contCeldas>-1 && contCeldas<3)){
            detallePorGrupo(row, key, valor,contCeldas)
          }
            switch (grupo){
              case 1:{
                if(key!=="-1" && (contCeldas>2 && contCeldas<18)){
                    detallePorGrupo(row, key, valor,contCeldas)
                  }
                break;
              }
              case 2:
                if(key!=="-1" && (contCeldas>17 && contCeldas<35)){
                  detallePorGrupo(row, key, valor,contCeldas)
                }
              break;
              case 3:
                if(key!=="-1" && (contCeldas>34 && contCeldas<52)){
                  detallePorGrupo(row, key, valor,contCeldas)
                }
              break;
              case 4:
                if(key!=="-1" && (contCeldas>51 && contCeldas<69)){
                  detallePorGrupo(row, key, valor,contCeldas)
                }
              break;
              case 5:
                if(key!=="-1" && (contCeldas>68 && contCeldas<86)){
                  detallePorGrupo(row, key, valor,contCeldas)
                }
              break;
              case 6:
                if(key!=="-1" && (contCeldas>85)){
                  detallePorGrupo(row, key, valor,contCeldas)
                }
              break;
            }
          }
        contCeldas++
      });
      if(contFilas>=3) {
        filas.push(row)
      }
      contFilas++;
    });
    return filas;
  }

  enviarJson(json){
    this.cargarService.cargar(json).subscribe(
      data => {
        this.toastr.success('Archivo cargado', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/']);
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }
}