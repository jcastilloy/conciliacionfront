import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {LoginUsuario} from '../../../models/login-usuario';
import {AuthService} from '../../../services/auth.service';
import { TokenService } from '../../../services/token.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLogged = false;
  isLoginFail = false;
  nombreUsuario: string;
  password: string;


  constructor(
    public authService: AuthService,
    public  router:  Router,
    private tokenService: TokenService
  ) { }

  ngOnInit(): void {
     if (this.tokenService.getToken()) {
      this.isLogged = true;
      this.isLoginFail = false;
      this.router.navigate(['/index']);
    }
  }

}
