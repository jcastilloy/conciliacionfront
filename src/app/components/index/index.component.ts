import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogService } from 'src/app/confirmation-dialog/confirmation-dialog.service';

import { Archivo } from '../../models/archivo';
import { ArchivoService } from '../../services/archivo.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  
  archivos: Archivo[]=[];
  
  constructor(
    private archivoService: ArchivoService,
    private confirmationDialogService: ConfirmationDialogService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.cargarArchivos();
  }

  cargarArchivos(): void{
    this.archivoService.lista().subscribe(
      data=>{
        this.archivos = data;
      }, err=>{
        console.log(err);
      }
    );

  }
  borrar(arch: Archivo){
    this.confirmationDialogService.confirm('Alerta!', 'Esta seguro de querer eliminar el archivo?')
    .then((confirm) => {
      if(confirm){
        this.archivoService.delete(arch).subscribe(
          data => {
            this.toastr.success('Archivo Eliminado', 'OK', {
              timeOut: 3000, positionClass: 'toast-top-center'
            });
            this.cargarArchivos();
          },
          err => {
            this.toastr.error('Error al eliminar el archivo', 'Fail', {
              timeOut: 3000, positionClass: 'toast-top-center',
            });
          }
        );
      }
    });    
  }

  detalle(){
    
  }
}
