import { Component, OnInit } from '@angular/core';
import { CargaExcelService } from 'src/app/services/carga-excel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


import * as XLSX from 'xlsx'; 

@Component({
  selector: 'app-detalle-archivo',
  templateUrl: './detalle-archivo.component.html',
  styleUrls: ['./detalle-archivo.component.css']
})
export class DetalleArchivoComponent implements OnInit {
  
 data: any[]=[];
  
  fileName= 'ExcelSheet.xlsx';

  constructor(
    private cargaService: CargaExcelService,
    private toastr: ToastrService, 
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.cargarDetalle();
  }

  cargarDetalle(): void{
    const id = this.activatedRoute.snapshot.params.id;
    this.cargaService.lista(id).subscribe(
      data =>{
        this.data = data.data;
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }

  getHeaders() {
    let headers: string[] = [];
    if(this.data) {
      this.data.forEach((value) => {
        Object.keys(value).forEach((key) => {
          if(!headers.find((header) => header == key)){
            headers.push(key)
          }
        })
      })
    }
    return headers;
  }

  exportar(): void{
     /* table id is passed over here */   
     let element = document.getElementById('excel-table'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     /* generate workbook and add the worksheet */
     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

     /* save to file */
     XLSX.writeFile(wb, this.fileName);
  }
}
